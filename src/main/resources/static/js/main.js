var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/', {
	      controller: 'GuestController',
	      templateUrl: 'views/main.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});