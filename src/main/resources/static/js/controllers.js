var app = angular.module('project');

app.controller('GuestController', function($scope, $http, $route, $location) {
	$scope.comments = [];
	$scope.imageURLlist = [];
	$scope.tempguest = [];
	$scope.repCount = 0;
	
	$http.get('/rest/book').success(function(data, status, headers, config) {  
		$scope.comments = data;
		angular.forEach($scope.comments,function(value,index) {
			$scope.imageURLlist.push(getRandomImage());
		});
		if(data.length > 0)
			$scope.showComments = true; 
		else
			$scope.showComments = false; 
		console.log($scope.comments);
	});
	
		
	$scope.createComment = function (type, linked) {
		var emailStatus = true;
		var emailText = "";

		if (type === -1){
			if($scope.email !== undefined) 
				emailText = $scope.email.text;
			
			guest = { name: $scope.name, email: emailText, comment: $scope.comment, linkedto: linked};
		}
		else {
			if($scope.tempguest[linked].email !== undefined) 
				emailText = $scope.tempguest[linked].email.text;
			
			console.log($scope.tempguest[linked]);
			guest = { name: $scope.tempguest[linked].name, email: emailText, comment: $scope.tempguest[linked].comment, linkedto: linked};
		
		}
		
		console.log(guest.email);
	    if (guest.email!==undefined && guest.email.trim().length > 0)
			emailStatus = validateEmail(guest.email);
		else if(guest.email===undefined)
			emailStatus = true;
		else
			emailStatus = true;
		
	
		if (guest.name!==undefined && guest.name!=="" && guest.comment!==undefined && guest.comment!=="" && emailStatus === true){
			$http.post('/rest/book/create', guest).success(function(data, status, headers, config) {			
				$http.get('/rest/book').success(function(data, status, headers, config) {  
					$scope.comments = data;
					angular.forEach($scope.comments,function(value,index) {
						$scope.imageURLlist.push(getRandomImage());
					});
					$scope.showComments = true; 
					guest = {};
					$scope.tempguest=[];
					$scope.name = "";
					$scope.email = "";
					$scope.comment = "";
				});
		    });
		}

	}
	
	$scope.count = function (orderNum) {
		var counter = 0;		
		angular.forEach($scope.comments,function(value,index) {
			if(value.linkedto === orderNum)
				counter = counter + 1;
		});
		return counter;
	}
	
	function getRandomImage() {
		var resultURL = "";
		var randomNumber = Math.floor(Math.random() * 14) + 1;	
		resultURL = "/images/" + randomNumber + ".jpg";
		return resultURL;
	}
	
	function validateEmail(inputEmail) {
		var filter = /[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}/igm;
		if (!filter.test(inputEmail))
	        return false;
	    else
	    	return true;
	}
});

function changetext(iText){
	var inpText = iText.substring(0,4), result="";
	if(inpText === "View") 
		result = iText.replace("View", "Hide"); 
	else if(inpText === "Hide")
		result = iText.replace("Hide", "View");	
	return result;
}

