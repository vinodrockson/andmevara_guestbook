package com.guestbook.controllers;



import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.guestbook.assemblers.GuestAssembler;
import com.guestbook.models.Guest;
import com.guestbook.repositories.GuestRepository;
import com.guestbook.resources.GuestResource;

@RestController
@RequestMapping(value="/rest/book")
public class GuestController {
	
	@Autowired
	GuestRepository grepo;
	
	GuestAssembler guestAssembler = new GuestAssembler();
	
    //create comment
    @RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GuestResource> createComment(@RequestBody GuestResource gres) throws Exception {
		if (gres == null) {
			return null;
		}
		
		Guest guest = new Guest();
		guest.setName(gres.getName());
		guest.setEmail(gres.getEmail());
		guest.setComment(gres.getComment());
		Long link_to = gres.getLinkedto();
		guest.setLinkedto(link_to);
		Long nextOrder = grepo.getCountOfComment(link_to);
		guest.setOrdernum(nextOrder + 1);

		Guest createdGuest = grepo.save(guest);
		GuestResource res = guestAssembler.toResource(createdGuest);
	    HttpHeaders headers = new HttpHeaders();
	    headers.setLocation(new URI(res.getId().getHref()));	    
    	return new ResponseEntity<>(res, headers, HttpStatus.CREATED);
    	
    }
 
    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseStatus(HttpStatus.OK)
    public List<GuestResource> getAllComments() {
    	List<Guest> guests = grepo.findAll();
    	List<GuestResource> guestResource = guestAssembler.toResource(guests);
    	return guestResource;
    }
 }
