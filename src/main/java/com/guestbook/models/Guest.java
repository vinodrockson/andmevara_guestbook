package com.guestbook.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Guest {

	@Id
	@GeneratedValue
	Long id;
	
	String name;
	String email;
	String comment;
	Long linkedto;
	Long ordernum;
	
}
