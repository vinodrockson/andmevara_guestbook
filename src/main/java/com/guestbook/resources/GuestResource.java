package com.guestbook.resources;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.guestbook.support.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GuestResource  extends ResourceSupport{
	
	Long idres;	
	String name;
	String email;
	String comment;
	Long linkedto;
	Long ordernum;
}
