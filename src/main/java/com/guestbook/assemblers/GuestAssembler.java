package com.guestbook.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.guestbook.controllers.GuestController;
import com.guestbook.models.Guest;
import com.guestbook.resources.GuestResource;

public class GuestAssembler extends ResourceAssemblerSupport<Guest, GuestResource> {
	
	public GuestAssembler() {
		super(GuestController.class, GuestResource.class);
	}

	@Override
	public GuestResource toResource(Guest guest) {

		GuestResource guestResource = createResourceWithId(guest.getId(), guest);

		guestResource.setName(guest.getName());
		guestResource.setEmail(guest.getEmail());
		guestResource.setComment(guest.getComment());
		guestResource.setLinkedto(guest.getLinkedto());
		guestResource.setOrdernum(guest.getOrdernum());
//		guestResource.add(new ExtendedLink(linkTo(methodOn(GuestController.class).delete(
//					equipment.getId())).toString(), "deleteGuest", "DELETE"));
//		}

		return guestResource;
	}

	public List<GuestResource> toResource(List<Guest> guests) {
		List<GuestResource> GuestList = new ArrayList<>();

		for (Guest g : guests) {
			GuestList.add(toResource(g));
		}
		return GuestList;
	}

}
