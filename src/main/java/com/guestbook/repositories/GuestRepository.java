package com.guestbook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.guestbook.models.Guest;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
	
		@Query("select count(comment) from Guest guest where guest.linkedto = :link")
		Long getCountOfComment(@Param("link") Long link);
}
